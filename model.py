import matplotlib.pyplot as plt
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import seaborn as sns
import numpy as np
from sklearn.model_selection import GridSearchCV

# database de treino

# temporadas 1996/1997 - 2015/2016
df_train = pd.read_csv("./ASG_train_split.csv")

# temporadas 2016/2017 - 2018/2019
df_test = pd.read_csv("./ASG_test_split.csv")

# database que queremos prever os resultados

df_to_predict = pd.read_csv('./ASG_to_predict.csv')  # temporada 2019/2020

print("df_to_predict: ", df_to_predict.head())

# usaremos para montar o csv com os resultados
names_and_teams = df_to_predict[['PLAYER', 'TEAM']]

# ano em que estamos querendo prever os resultados
prediction_year = df_to_predict.loc[0, 'Year'] + 1

# quantidade total de linhas e colunas da nossa base de dados
print("Linhas: %d, Colunas: %d" % (len(df_train), len(df_train.columns)))

# lista de jogadores que foram selecionados para o All Star Game sem necessariamente terem "merecido"
# Na maioria dos casos trata-se de jogadores extramente famosos em sua ultima temporada
# precisamos retirar esses casos para não atrapalhar o nosso treinamento
# Esses foram todos os casos mais significativos que conseguimos encontrar
outliers = set([
    ('Dirk Nowitzki', 2018),
    ('Kobe Bryant', 2015),
    ('Kobe Bryant', 2013),
    ('Yao Ming', 2010),
    ('Allen Iverson', 2009),
    ('Allen Iverson', 2008),
    ("Shaquille O'Neal", 2006),
    ('Ray Allen', 2003),
    ('Jamaal Magloire', 2003),
    ('Vince Carter', 2002),
    ('Grant Hill', 2000),
    ('Anfernee Hardaway', 1997),
    ('Anfernee Hardaway', 1996)
])

# guardando em um vetor
outlier_indices = []


def outliers_process(row):
    if (row['PLAYER'], row['Year']) in outliers:
        outlier_indices.append(row.name)


# excluindo os casos pertencentes aos "outliers"
df_train[['PLAYER', 'Year']].apply(outliers_process, axis=1)
df_train.drop(outlier_indices, inplace=True)

features = [
    'PTS',
    'REB',
    'AST',
    'BLK',
    'DEFWS',
    'USG%',
    'PIE',
    'Team Conference Rank',
    'Prior ASG Appearances',
    'AS Last Year?'
]

print("FEATURES")
print(features)

# Dividindo a base de dados em treino e teste

# x = df_train[features].values
# y = df_train['Selected?'].values

x_train = df_train[features].values
y_train = df_train['Selected?'].values

x_test = df_test[features].values
y_test = df_test['Selected?'].values

# metodo de seleção aleatória dos dados de treino e de teste
# x_train, x_test, y_train, y_test, = train_test_split(x, y, test_size=0.3)

# Aplicando pca

pca = PCA(n_components=2)

x_pca = df_train[features].values
y_pca = df_train['Selected?'].values

x_pca = StandardScaler().fit_transform(x_pca)

husl = sns.color_palette('husl')
# 0 = non all-star: red
# 1 = all-star: green
palette = {
    0: husl[0],
    1: husl[3]
}

components = pca.fit_transform(x_pca)
df_pca = pd.DataFrame(data=components, columns=['Principal Component 1',
                                                'Principal Component 2'])
df_pca['Result'] = y_pca

explained_variance = 100*sum(pca.explained_variance_ratio_)

fig, ax = plt.subplots(figsize=(10, 10))
sns.scatterplot(ax=ax, data=df_pca, x='Principal Component 1', y='Principal Component 2',
                markers=['v', 'o'], style='Result', hue='Result', palette=palette)
ax.set_title(
    '2-Component PCA (Explained Variance: {:.2f}%)'.format(explained_variance), size=20)
new_labels = ['Not All-Star', 'All-Star']
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles=handles[1:], labels=new_labels)
plt.savefig('./plots/PCA.png')
plt.show()

print("### RANDOM FOREST ###")

# Treinando o modelo

random_classifier = RandomForestClassifier(
    random_state=0, criterion='gini', n_jobs=-1)

parameters = {'bootstrap': [True], 'max_features': np.arange(5, 10), 'n_estimators': [
    500], 'min_samples_leaf': [10, 50, 100, 200, 500]}

classifier_rf = GridSearchCV(
    estimator=random_classifier, param_grid=parameters, cv=5, n_jobs=-1)

classifier_rf.fit(x_train, y_train)

# Best params

print("best_params: ", classifier_rf.best_params_)

# Ao passar a entrada de teste "x_pred", o método "predict", irá tentar prever a saída de acordo com o aprendizado do modelo

y_pred = classifier_rf.predict(x_test)

print("Y_pred: ", y_pred)

# Analisando os resultados

# Acuracia é qualculada analisando a saída de teste "y_test", que é um dado que já temos, junto com a saída calculada pelo modelo "y_pred"

# Accuracy

print("Accuracy: ", accuracy_score(y_test, y_pred))

# Feature Importance

print("### FEATURE IMPORTANCE ###")

random_classifier.fit(x_train, y_train)

features_importance = zip(random_classifier.feature_importances_, features)
for importance, feature in sorted(features_importance, reverse=True):
    print("%s: %f%%" % (feature, importance*100))

# Aqui estamos calculando a probabilidade da saída estar correta de acordo com os dados de entrada
# O próprio Random Forest e o GridSearchCV já possuem esse método por padão

# y_test_proba = [prob[1] for prob in classifier_rf.predict_proba(x_test)]
# print("y_test_proba: ", y_test_proba)
# printamos para não "sujar" o terminal com as informações mais importante que queremos ver

# Aqui estmos especificando, dentro do CSV que iremos tentar prever os resultados, quais colunas usaremos

df_to_predict = df_to_predict[features]

# Estamos criando uma coluna detrno do CSV que contém os dados de entrada que estamos tentando prever os resultados
# e inserindo nessa coluna nova a probabilidade de acerto do nosso modelo em tentar prever a sua classificação

df_to_predict['AS Prob.'] = [prob[1]
                             for prob in classifier_rf.predict_proba(df_to_predict)]

df_to_predict = df_to_predict.join(names_and_teams)

eastern_conference = set(['ATL', 'BOS', 'BKN', 'CHA', 'CHI', 'CLE', 'DET', 'IND', 'MIA',
                          'MIL', 'NYK', 'ORL', 'PHI', 'TOR', 'WAS'])
df_to_predict['Conf.'] = df_to_predict['TEAM'].map(
    lambda x: 'East' if x in eastern_conference else 'West')

df_east = df_to_predict[df_to_predict['Conf.'] == 'East'].sort_values(
    'AS Prob.', ascending=False).reset_index(drop=True)
df_west = df_to_predict[df_to_predict['Conf.'] == 'West'].sort_values(
    'AS Prob.', ascending=False).reset_index(drop=True)

for df in [df_east, df_west]:
    df['Model Prediction'] = df.index.map(
        lambda x: 'All Star' if x < 12 else 'Not All Star')

df_east = df_east[['PLAYER', 'TEAM', 'Conf.', 'AS Prob.', 'Model Prediction']]
df_west = df_west[['PLAYER', 'TEAM', 'Conf.', 'AS Prob.', 'Model Prediction']]

df_results_full = pd.concat([df_east, df_west]).set_index(
    'PLAYER').sort_values('AS Prob.', ascending=False)

# Predições para o All-Star Game

df_results_full.to_csv(
    '{}_NBA_ASG_predictor.csv'.format(prediction_year))
