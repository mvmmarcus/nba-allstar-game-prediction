<h1 align="center">🔗 NBA AllStar Game Prediction</h1>

<p id="sobre" align="center">🚀 Projeto em Machine Learn que tem o objetivo de tentar prever, utilizando ferramentas como RandomForest, Principal Component Analysis (PCA), GridSearchCV, entre outros, os atletas que participarão do famoso All Star Game (Jogo das estrelas) da NBA.</p>

<div style="width: 100%; align-items: center; text-align: center;" >
<img align="center" alt="GitHub Pipenv locked Python version" src="https://img.shields.io/github/pipenv/locked/python-version/metabolize/rq-dashboard-on-heroku"/>
<a href="https://www.emc.ufg.br/" ><img align="center" alt="GitHub Pipenv locked Python version" src="https://img.shields.io/static/v1?label=UFG&message=EMC&color=rgb(0 103 172)&style=social&logo=data:image/svg%2bxml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDIwMDEwOTA0Ly9FTiIKICJodHRwOi8vd3d3LnczLm9yZy9UUi8yMDAxL1JFQy1TVkctMjAwMTA5MDQvRFREL3N2ZzEwLmR0ZCI+CjxzdmcgdmVyc2lvbj0iMS4wIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiB3aWR0aD0iODE2LjAwMDAwMHB0IiBoZWlnaHQ9IjEwMTYuMDAwMDAwcHQiIHZpZXdCb3g9IjAgMCA4MTYuMDAwMDAwIDEwMTYuMDAwMDAwIgogcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQgbWVldCI+Cgo8ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjAwMDAwMCwxMDE2LjAwMDAwMCkgc2NhbGUoMC4xMDAwMDAsLTAuMTAwMDAwKSIKZmlsbD0iIzAwMDAwMCIgc3Ryb2tlPSJub25lIj4KPHBhdGggZD0iTTUxMDAgOTM2OSBjLTExMyAtNjcgLTI2MyAtMTU2IC0zMzUgLTE5OCAtNzEgLTQzIC0yNDQgLTE0NCAtMzg0Ci0yMjYgbC0yNTQgLTE0OSA2IC0xOTUgYzQgLTEwNyA3IC00MTIgNyAtNjc4IGwwIC00ODMgNDMgLTI2IGMyMyAtMTMgMTI4IC03MwoyMzIgLTEzMiAxMDUgLTYwIDM0NyAtMTk3IDUzOSAtMzA2IDE5MiAtMTA5IDM1NiAtMjAxIDM2NiAtMjAzIDEwIC0zIDUxIDE1CjkxIDM5IDQxIDI1IDEyOCA3NiAxOTQgMTE1IDUwNyAyOTggNzcxIDQ1MyA4NDggNDk3IGw0NyAyNyAwIDM2OCBjMCAyMDIgLTMKNTEwIC03IDY4NCBsLTYgMzE2IC0xNjYgOTUgYy0zNzUgMjEzIC0xMDE0IDU3NiAtMTAxNSA1NzYgMCAtMSAtOTMgLTU1IC0yMDYKLTEyMXoiLz4KPHBhdGggZD0iTTI5NjQgODM2MyBjLTE1MCAtODggLTM3MCAtMjE2IC01MDEgLTI5NCBsLTkzIC01NCAwIC00MjIgMCAtNDIxIDc4Ci00NSBjNDIgLTI1IDEwMCAtNTcgMTI3IC03MyAyOCAtMTUgMTU1IC04NyAyODQgLTE2MSAxMjggLTczIDIzNyAtMTMzIDI0MwotMTMzIDUgMCA2OSAzNSAxNDEgNzcgMTM1IDc5IDE4NSAxMDggNDM3IDI1NSBsMTQ1IDg1IDMgNzQgYzIgNDEgMSAyMzIgLTMKNDI0IGwtNyAzNTEgLTMyMiAxODIgYy0xNzYgMTAwIC0zMzkgMTkzIC0zNjIgMjA3IGwtNDAgMjQgLTEzMCAtNzZ6Ii8+CjxwYXRoIGQ9Ik01OTE1IDY5MjkgYy0xMDQgLTYxIC0yNjQgLTE1NSAtMzU1IC0yMDkgLTkxIC01NCAtMTc1IC0xMDMgLTE4NwotMTA5IC0yMyAtMTIgLTIzIC0xMiAtMjIgLTI0OSAxIC0xMzAgNSAtMzI5IDggLTQ0MiBsNiAtMjA0IDEzNSAtNzYgYzc0IC00MgoyNDQgLTEzOCAzNzcgLTIxNCAyMTkgLTEyNCAyNDQgLTEzNiAyNjUgLTEyNSAxMyA3IDEzMyA3NyAyNjggMTU3IDEzNSA3OSAyOTYKMTc0IDM1OCAyMTEgOTUgNTUgMTEyIDY5IDEwOCA4NiAtMyAxMSAtNiAyMDcgLTYgNDM2IGwwIDQxNyAtMTcyIDk3IGMtOTUgNTQKLTE5OCAxMTMgLTIyOCAxMzAgLTEwMyA2MSAtMzU4IDIwNSAtMzYxIDIwNSAtMiAwIC04OSAtNTAgLTE5NCAtMTExeiIvPgo8cGF0aCBkPSJNMTk5MCA2OTMxIGMtMjM3IC0xNDEgLTUwMCAtMjk2IC02MTAgLTM1OSAtNTIgLTMwIC05NyAtNTggLTEwMCAtNjIKLTMgLTUgLTMgLTI0MSAwIC01MjUgbDUgLTUxNyAxNzAgLTk3IGMxNjEgLTkyIDYxOSAtMzUxIDY5NiAtMzk0IGwzNSAtMjAgMTA1CjYwIGM5NyA1NiAxNjggOTcgNTQyIDMxNyA3OCA0NiAxNjAgOTQgMTgyIDEwNiAyMiAxMyA0NyAyOSA1NSAzNiAxMyAxMSAxNCA3OAoxMCA1MzAgbC01IDUxNyAtMTYwIDkxIGMtNTQ5IDMxNCAtNzIxIDQxMCAtNzQwIDQxMiAtMTEgMiAtOTAgLTM5IC0xODUgLTk1eiIvPgo8cGF0aCBkPSJNNTE4MCA1NDQyIGMtMzYgLTIxIC0xNDQgLTg1IC0yNDAgLTE0MSAtOTYgLTU3IC0yMjIgLTEzMSAtMjgwIC0xNjUKLTU4IC0zNCAtMTUxIC04OSAtMjA3IC0xMjIgbC0xMDMgLTU5IDAgLTE3NSBjMCAtOTYgMyAtMzMzIDcgLTUyNiBsNiAtMzUyCjMxNSAtMTc4IGMxNzMgLTk5IDM2MSAtMjA1IDQxNiAtMjM3IDU2IC0zMiAxMTkgLTY3IDE0MSAtNzggbDQwIC0yMCAzMTAgMTgyCmMxNzEgMTAwIDM3NSAyMjAgNDU0IDI2NyBsMTQ0IDg1IC02IDE1MCBjLTQgODMgLTcgMzE4IC03IDUyMiBsMCAzNzIgLTIxNwoxMjQgYy00OTkgMjgzIC02ODkgMzg5IC02OTggMzg5IC01IDAgLTM5IC0xNyAtNzUgLTM4eiIvPgo8cGF0aCBkPSJNMzUwNSA1MTYyIGMtNTUgLTMyIC0xODcgLTExMCAtMjkzIC0xNzIgbC0xOTIgLTExNCA2IC0zMzYgYzQgLTE4NAo5IC0zMzggMTMgLTM0MiAxMCAtMTAgNTcwIC0zMjggNTc5IC0zMjggNCAxIDEzNSA3NiAyOTIgMTY4IGwyODUgMTY3IDMgMzM3IDIKMzM4IC0zNyAyNCBjLTQxIDI2IC01NDkgMzE2IC01NTUgMzE2IC0yIDAgLTQ4IC0yNiAtMTAzIC01OHoiLz4KPHBhdGggZD0iTTU5NjAgMjgxMCBjLTUzMCAtOTYgLTg1NyAtNDI4IC05MjEgLTkzNiAtMTQgLTExNCAtNyAtMjc1IDE2IC0zODkKNjYgLTMxOCAyMzEgLTU0MiA1MDAgLTY3NyAzMDMgLTE1MyA3NzkgLTE2OCAxMjMwIC00MCBsMTAwIDI5IDAgNTU0IDAgNTU0Ci0zOTAgMCAtMzkwIDAgLTMgLTE4MiBjLTIgLTE2OSAtMSAtMTgzIDE1IC0xODMgMTAgLTEgODMgLTIgMTYzIC0zIGwxNDUgLTIgMwotMjIyIDIgLTIyMiAtNTYgLTExIGMtODUgLTE4IC0yNzggLTE1IC0zNjAgNSAtMjMyIDU3IC0zOTUgMjE4IC00NjAgNDU1IC0yOAoxMDIgLTI2IDMzMCA0IDQzNSA1MSAxNzggMTU1IDMwNiAzMTAgMzg0IDEyMyA2MSAyMDAgNzYgMzkyIDc2IDE3OCAtMSAyODAKLTE2IDM5NSAtNjAgbDYzIC0yMyAyMSA3NiBjNTIgMTk3IDczIDI5MSA2OCAzMDUgLTYgMTYgLTExNiA0OCAtMjQ5IDc0IC01MwoxMCAtMTU3IDE3IC0yODggMTkgLTE2OSAzIC0yMjMgMCAtMzEwIC0xNnoiLz4KPHBhdGggZD0iTTEzNDcgMjgwMyBjLTEwIC05IC04IC0xMDI4IDMgLTEyMzggNSAtMTEyIDE2IC0yMTUgMjkgLTI3MiA3MCAtMzE5CjI0OSAtNTE4IDUzNiAtNTk1IDY4IC0xOCAxMDggLTIxIDI3MCAtMjIgMjI0IDAgMzAxIDE1IDQ1NCA5MCA3NyAzOCAxMDkgNjEKMTc1IDEyOCA5NiA5NiAxMzcgMTYzIDE4MiAyOTQgNjIgMTgwIDY0IDIxNCA2NCA5NTUgbDAgNjY3IC0yMzQgMCAtMjM1IDAgLTQKLTY5MiBjLTQgLTc1NiAtMyAtNzUwIC02MyAtODczIC0zOSAtNzkgLTc1IC0xMTcgLTE0NiAtMTU2IC02MCAtMzIgLTY3IC0zNAotMTY4IC0zNCAtODkgMCAtMTEzIDQgLTE1NyAyNCAtNjYgMzAgLTEzNiAxMDMgLTE3MyAxODQgLTU2IDEyMCAtNjAgMTczIC02MAo4OTMgbDAgNjU0IC0yMzMgMCBjLTEyOSAwIC0yMzcgLTMgLTI0MCAtN3oiLz4KPHBhdGggZD0iTTM0ODIgMTc1OCBsMyAtMTA1MyAyMzUgMCAyMzUgMCAzIDQyMyAyIDQyMiAzNzUgMCAzNzUgMCAwIDE5NSAwCjE5NSAtMzc1IDAgLTM3NSAwIDAgMjQwIDAgMjQwIDQwMyAyIDQwMiAzIDMgMTkzIDIgMTkyIC02NDUgMCAtNjQ1IDAgMiAtMTA1MnoiLz4KPC9nPgo8L3N2Zz4K"/></a>
</div>

###

<p align="center">
 <a href="#-sobre">Sobre</a> •
 <a href="#-requisitos">Pré-requisitos</a> • 
 <a href="#-tecnologias">Tecnologias</a> • 
 <a href="#-resultados">Resultados</a> • 
 <a href="#-instalacao">Instalação</a> •
 <a href="#-contribuicoes">Contribuições</a> 
</p>

###

## Pré-requisitos

<p id="requisitos" >Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Python](https://www.python.org/).
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)</p>

###

## 🛠 Tecnologias

<div id="tecnologias" >
As seguintes ferramentas foram usadas na construção do projeto:

- [Python](https://www.python.org/)
- [Pandas](https://pandas.pydata.org/)
- [RandomForest](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html)
- [GridSearchCV](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html)
- [PCA](https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html)
</div>

###

## Resultados

<div id="resultados" >
<img src="./assets/results.png" alt="results" />
</div>

Após alguns testes, modificações e melhorias, o modelo está conseguindo prever cerca de 70% dos atletas que participarão ou participaram (para temporadas que já passaram) do All Star Game da NBA.

###

## 🚀 Como executar o projeto

<div id="instalacao" >

### Clone este repositório
$ git clone git@gitlab.com:mvmmarcus/nba-allstar-game-prediction.git

### Acesse a pasta do projeto no terminal/cmd
$ cd/nba-allstar-game-prediction

### Instale as dependências
$ pip install -r requirements.txt

### Execute o arquivo model.py
$ python model.py
</div>

###

## 👨‍💻 Contribuidores

Um agradecimento especial ao <a href="https://sites.google.com/site/sandrerley" >Sandrerley Ramos Pires</a>, que foi nosso orientador nesse projeto incrível 👏

<table id="contribuicoes" >
  <tr>
    <td align="center"><a href="https://gitlab.com/mvmmarcus"><img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/6195744/avatar.png?width=400" width="100px;" alt=""/><br /><sub><b>Marcus Vinícius</b></sub></a><br /><a href="https://gitlab.com/mvmmarcus" title="Marcus Vinicius">👨‍🚀</a></td>
    <td align="center"><a href="https://github.com/vhmvictor"><img style="border-radius: 50%;" src="https://avatars0.githubusercontent.com/u/51443108?s=400&u=d15d6338a528381285ff58cc27ad360e415c98b1&v=4" width="100px;" alt=""/><br /><sub><b>Victor Hugo</b></sub></a><br /><a href="https://github.com/vhmvictor" title="Victor Hugo">👨‍🚀</a></td>
  </tr>
</table>

Desenvolvido com ❤️ por <a href="https://gitlab.com/mvmmarcus">Marcus Vinícius</a> e <a href="https://github.com/vhmvictor">Victor Hugo</a>
